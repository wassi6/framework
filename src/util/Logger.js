const chalk = require('chalk');
const moment = require('moment');
const util = require('util');

class Logger {
	log(content, { color = 'grey', tag = 'Log' } = {}) {
		this.write(content, { color, tag });
	}

	info(content, { color = 'green', tag = 'INFO' } = {}) {
		this.write(content, { color, tag });
	}

    warn(content, { color = 'yellow', tag = 'WARN' } = {}) {
		this.write(content, { color, tag });
	}

	error(content, { color = 'red', tag = 'ERROR' } = {}) {
		this.write(content, { color, tag, error: true });
	}

	stacktrace(content, { color = 'white', tag = 'ERROR' } = {}) {
		this.write(content, { color, tag, error: true });
	}

	write(content, { color = 'grey', tag = 'Log', error = false } = {}) {
		const timestamp = chalk.cyan(`[${moment().utcOffset('+03:00').format('MM/DD HH:mm')}]`);
		const levelTag = chalk[color].bold(`❯${tag}:`);
		const text = chalk[color](this.clean(content));
		const stream = error ? process.stderr : process.stdout;
		stream.write(`${timestamp} ${levelTag} ${text}\n`);
	}

	clean(item) {
		if (typeof item === 'string') return item;
		const cleaned = util.inspect(item, { depth: Infinity });
		return cleaned;
	}
}

module.exports = Logger;