const { AkairoClient } = require('../../src/index');

class TestClient extends AkairoClient {
    constructor() {
        super({
            prefix: '!!',
            ownerID: '123992700587343872',
            commandDirectory: './test/commands/',
            handleEdits: true,
            allowMention: true
        });

    }

    setup() {
        const resolver = this.commandHandler.resolver;
        resolver.addType('1-10', word => {
            const num = resolver.type('integer')(word);
            if (num == null) return null;
            if (num < 1 || num > 10) return null;
            return num;
        });
    }

    start(token) {
        this.setup();
        return this.settings.init()
            .then(() => this.login(token))
            .then(() => console.log('Ready!')); // eslint-disable-line no-console
    }
}

module.exports = TestClient;
